---
title: ActivityScope User Interface
keywords: userinterface, ActivityScope
tags: [getting_started]
summary: ""
sidebar: mydoc_sidebar
permalink: user-interface.html
folder: docs
---

<style TYPE="text/css">
code.has-jax {font: inherit; font-size: 100%; background: inherit; border: inherit;}
</style>
<script type="text/x-mathjax-config">
MathJax.Hub.Config({
    tex2jax: {
        inlineMath: [['$','$'], ['\\(','\\)']],
        skipTags: ['script', 'noscript', 'style', 'textarea', 'pre'] // removed 'code' entry
    }
});
MathJax.Hub.Queue(function() {
    var all = MathJax.Hub.getAllJax(), i;
    for(i = 0; i < all.length; i += 1) {
        all[i].SourceElement().parentNode.className += ' has-jax';
    }
});
</script>
<!-- <script type="text/javascript" src="http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script> -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.3/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>

![alt text](./images/activityscope_logo.png "ActivityScope")

## User Manual ##

---

## Orientation Diagram ##

![alt text](./images/overview.png "Interface Overview")

## Participant Meta Data ##

This section displays the additional participant details manually inputted by the user at the collection stage.

## Main Graph Type ##
The button toggles the main graph between a time-series chart or a Heatmap (default). Both the time-series chart and heatmap are colour coded as per the [cutpoint](#cutpoints) thresholds:

+ <span style="color:red">**Red: Vigorous**</span>
+ <span style="color:orange">**Yellow: Light-Moderate**</span>
+ <span style="color:green">**Green: Sedentry**</span>

![alt text](./images/linegraph.png "line graph view")

For periods where no data is collected (**holes**), no data is plotted. For periods where data is collected but no wear is detected, the plot is collored in <span style="color:grey">** Grey**</span>. Non-wear is calculated as per methods described in [13](#ref13) which have been tested to function well in healthy canine populations.

### VM3 Counts ###
Both graphs are fed by **VM3 Count** data (VM3: 3-axis vector magnitude). This is a euclidean vector sum of counts in each of the anatomical axis of the wearer such that:

$$\text{VM3} = \sqrt{AP^2_{counts}+ML^2_{counts}+DV^2_{counts}}$$

> AP = counts in anterior-posterior plane, ML = counts in medial-lateral plane, DV = counts in dorsal-ventral plane

Counts in each plane are calculated according to methods described in [1](#ref1)

The rest of the interface is reactive to the main graph and any selection made within it. If there is a specific area of interest, a *brush* can be drawn around it and the *Selection's* are reactive to the brushed data.

## Export ##
The **Export** button provides a direct download to all results from the  ActivityScope analysis. It is provided for doing further statistical analysis or research outside of the interface.

> NOTE: Once the data has been exported, it is up to the user to keep backups of the data and to enforce any access control or security policy. Any files on a local disk or network drive are not under version control, access control or part of any backup routine provided by VetSens.

The format of the exported data is in comma separated values (CSV) file. Headings are provided for each column and some packages may specify to ignore or remove these.

In Excel, a screen-shot of the data looks like

![alt text](./images/exported-data.png "exported data in Excel")

The columns are as follows

|**Heading**|**Desc**|**Units**|**REF**|
|-----------|:-------|:-------:|:-----:|
|Timestamp|Time at start of epoch|ISO8061 timestamp| - |
|AP Counts|Counts in Anterior-Posterior plane| counts/epoch |[1](#ref1)|
|ML Counts|Counts in Medial-Lateral plane| counts/epoch |[1](#ref1)|
|DV Counts|Counts in Dorsal-Ventral plane| counts/epoch |[1](#ref1)|
|VM3 Counts|Euclidean vector sum of AP,ML,DV counts| counts/epoch |[1](#ref1)|
|PA| Energy expended due to physical activity| kcals/epoch |[2](#ref2),[3](#ref3),[4](#ref4), [5](#ref5), [6](#ref6) |
|MER Counts|Maintenance Energy Requirement|kcals/epoch|[6](#ref6),[7](#ref7),[8](#ref8)|
|Distance|Distance walked| m/epoch| [9](#ref9),[10](#ref10),[11](#ref11),[12](#ref12)|
|Steps|Number of steps taken|steps/epoch|[9](#ref9),[10](#ref10),[11](#ref11),[12](#ref12)|
|SVM| Signal Vector Magnitude (with gravity component removed)$\dagger$ | g/epoch|[2](#ref2)|
|Worn| Detectted wear|1= worn, 0=not-worn|[13](#ref13)|
|UNIX Timestamp|number seconds since 1-1-1970 00:00:00 |`INT32`|-|

>$\dagger$ A 4th order band-pass (0.5-2.5Hz) filter is applied to the SVM to remove mechanical noise artifacts. Details of filter design are described in [1](#ref1)

## Share Option ##
The share button can be used to give a read only access to a particular set of results. This option is designed for users to give owners or interested parties access to the results. Upon clicking the share button, you will be required to enter the email address of the recipient.

## Settings ##

Settings can be used to change various behaviors of the interface.

## Selection Filters ##
Use the selection filters to change the time-span of the data shown in the [main graph](#graphtype)

## Bouts ##
Not all movement has cardiovascular health benefits. For example, scratching for 10 seconds does very little to raise the heart rate yet requires fast and vigorous movements. To make useful comparisons of movement based activity, it is sometimes desirable to filter movements that have short duration; just keeping bouts of sustained movement; the **Bouts** pane provides this utility. Bouts are classified into two groups:**Active Bouts** and **Inactive Bouts**. Active contains any movement of **Vigorous** or **Light-Moderate** intensity as classified by [14](#ref14) (also see [Cutpoints](#cutpoints) section). **Inactive Bouts** contain any other periods detected as worn. For a bout to be correctly classified, it has to be in *sustained, continuous intensity for a duration of 180 seconds (3 mins)*. If the activity drops below the threshold, the bout will end. A new bout will be subsequently started if the activity then rises again above the threshold for a period of 180 seconds.

Bouts are calculated over 3 periods:

* The period **Selected** in the [main graph](#graphtype)
* The **Daily** average (derived as a mean normalised to recording duration)
* The **Total** average (derived as the un-normailsed mean)

## Pedometer ##
Pedometer tab is used to give step counts of the fore-limbs, distance  traveled and duration of detected locomotion.

>NOTE:: The pedometer **NOT** a full gait analysis tool and does not make allowance for changes in step pattern or length due to changes between gaits, e.g. walk->trot->gallop.

The definition of a **Step** is taken as **the single movement of a limb for the purpose of intentional forward progress**. It is important not to confuse this metric with a stride. The definition of a **stride** is taken as **one complete cycle of a limb; from initial-contact to initial-contact**. When mounted near the head (e.g. on a collar or neck harness), the movement data analysed by the pedometer algorithm pertains to thoracic-limb steps. Typical

Full description of the step detection algorithm is detailed in [9](#ref9)

It is important to understand the Pedometer algorithm used has a **shuffle detection filter** and a **rhythmic walking filter**. This means some steps that do not fit the criterion are rejected. In addition, it is also important to understand the **walk duration** metric. This is a cumulative sum of all the time spent walking. In this way it can be used in conjunction with the selection brush on main graph to determine average speed for a particular time period:

![alt text](./images/pedometer_calculation.png "using selection tool with pedometer")
```
Distance Traveled = 2.24km
Time Taken = 1:51 (1.85hours)
Average Speed = 2.24/1.85
> 1.21km/h
```

In the above example it would thus appear in the period selected there was vigorous activity and a lot of steps (4506) but a relatively low walking speed. This would indicate a high probability of *Play* type activity.

## Energy Expenditure ##
The body maintains its weight through what is known as energy balance. In broad terms, the energy released through metabolism of food is used for three purposes:

- Maintaining bodily function (basal metabolism)
- Diet induced thermo-genesis (keeping warm)
- Movement or physical activity

>Imbalance in the energy balance relationship results in either weight gain or loss.

Typically, energy reserved for physical activity represents ~20-30% of available budget (thermo-genesis and resting basal metabolic rate is require ~10% and ~60-70% respectively). Through measuring movement intensity via the accelerometer, it is possible to estimate (in calorific terms) the energy expired through physical activity; more vigorous activities require more calories and result in higher-energy signals.

Energy Expenditure pane provides three outputs; a Maintenance Energy Requirement (**MER**), Resting Energy Requirement (**RER**), and Physical Activity component (**PA**).  These parameters are useful for tailoring dietary intake, monitoring exercise and understanding weight gains/losses. The time distribution can also provide insights diet induced behavioral abnormalities.

>Time distribution must be sought from a data [export](#export))

Several studies have examined the relationship body weight has on energy expenditure and a popular relationship is expressed in [5](#ref5) as:

$$ \text{E} = 767 \cdot  \text{X}^\text{-0.1408}$$

where E is in units of Kjoules/kg of $\text{bodyweight}^\text{0.67}$ and X is age in years.

This model is often used to estimate the energy intake required for weight maintenance.

### Energy from actigraphy ###
The energy estimates derived from the accelerometer are driven by the following assumption; Energy expended through Physical Activity is driven through muscle contractions, which in turn are manifested into movements that can be detected with the sensor.

>Limitations and if the accelerometer is ill placed or attached it cannot hope to capture these movements. Furthermore, elevation is not detected in this approach, so increased work done because of inclines is not registered

The relationship between accelerometer signals and Energy Expenditure (in calorific terms) has been studied previously [3](#ref3). This particular work suggests the relationship between Maintenance Energy Requirement (MER) and sensor data summarised into epochs containing SVM. Limitations of the [3](#ref3) are that only a generalised model is suggested to cover all breeds. From a meta analysis [8](#ref8), it is understood several factors could potentialy effect the MER of a dog:

- morphology
- husbandary
- breed size
- breed class
- neutered status
- activity level
- age

The work in [8](#ref8) suggests multiple equations for MER that are a best fit for each variant of the above factors. The results from the **Energy Expenditure** algorithm are based around a linear-regressed bet fit for the breed size equation set.

>It was thought this classification was the most pragmatic and most likely to be self selected by a non-clinical owner. It also handles mixed breeds gracefully.

Size classifications are as follows:

- **toy class:** 1>= AND <5Kg
- **small class:** 5>= AND <11Kg
- **medium class:** 11>= AND <19Kg
- **large class:** 19>= AND <34Kg
- **giant class:** >=34Kg

Each size class has a parameter set that can be applied to the generalised model:

$$ \text{MER} = ((x_1 * \text{BW}^{0.75}) +  x_2*\text{SVM})+ \text{intercept} $$

Where BW is body weight and EE is expressed in terms of $\text{BW}^{0.75}$

>Note: When finding MER from activity, the SVM must be aggregated in 60 second epochs. (normalised against a base sample frequency $\text{fs}=10\text{hz}$) and high-pass filtered to remove gravity as described in [3](#ref3)


### Energy balance for weight maintenance ###
For weight maintenance, energy metabolised and energy expended should be equal. Any extra or less will result in weight gain or loss. From **equation 1** we can construct the balance equation and subsequently calculate energy deficit or abundance.

 $$ \text{PA} =  \text{MER} - \text{RER}$$

Here $\text{RER}$ is the Resting Energy Requirement (thermo-genesis + basal metabolism) and can be expressed as defined in [5](#ref5):

 $$ \text{RER} =  70\text{kcal per kg}\cdot \text{BW}^{0.75}$$

>Note: The units of RER are in kCal per day.


## Cut Points ##
When examining behavioral based physical activity it is often useful to describe trends on a course basis as more granular measures may require mitigating context . Cutpoints are used to describe time spent in a physical activity state generally accepted to describe the coarse everyday activities levels encountered in life: Sedentary; Light-Moderate; and Vigorous. Of course these states are individualised and specific to age, breed and species. Results from [14](#ref14), [15](#ref15) and [16](#ref16) suggest typical parameters that are suitable for use on a generalised basis with reasonable accuracy. These parameters form the default ones used, however they can be changed in the [Settings](#settings) section.

>The underlying data being exposed to the cut-point approach is [VM3](#vm3) counts.

Cut Points are calculated over 3 periods:

* The period **Selected** in the [main graph](#graphtype)
* The **Daily** average (derived as a mean normalised to recording duration)
* The **Total** average (derived as the un-normailsed mean)

## Numerical Data ##
Data provided in [Bouts](#bouts), [Pedometer](#pedometer), [Energy Expenditure](#energy) and [Cut Points](#cutpoints) panes are provided in graphical form and tabular form. Switching between the two is done by selecting the table button.

## Sensor Attachment ##
All components of the ActivityScope analysis rely on proper sensor attachment. Sensors should be:

* Attached to a **SEPARATE COLLAR** than the leash (often a second collar is necessary)
* Position in the **VENTRAL** position
* Allow **2-FINGERS** gap for **TIGHTNESS**
* Ensure **LEDS** are facing **UPWARDS**
* Ensure **LOGO** is **VISIBLE**

[alt text](./images/sensor_collar.png "correct position on collar")

# References #
<a name="ref1"></a>[1] van Hees, V.T. Pias, M. Taherian, S. Ekelund, U. Brage, S. *A method to compare new and traditional accelerometry data in physical activity monitoring*. World of Wireless Mobile and Multimedia Networks (WoWMoM), 2010 IEEE International Symposium on a , vol., no., pp.1,6, 14-17 June 2010.

<a name="ref2"></a>[2] Esliger DW, Rowlands AV, Hurst TL, Catt M, Murray P, Eston RG *Validation of the GENEA Accelerometer*. . Med Sci Sports Exerc. 2011 Jun;43(6):1085-93.

<a name="ref3"></a>[3] Wrigglesworth DJ, Mort ES, Upton SL, Miller AT. *Accuracy of the use of triaxial accelerometry for measuring daily activity as a predictor of daily maintenance energy requirement in healthy adult Labrador Retrievers.* Am J Vet Res. 2011 Sep;72(9):1151-5. doi: 10.2460/ajvr.72.9.1151.

<a name="ref4"></a>[4] Bersch, Sebastian D. et al. *Sensor Data Acquisition and Processing Parameters for Human Activity Classification.* Sensors (Basel, Switzerland) 14.3 (2014): 4239–4270. PMC. Web. 13 Dec. 2016.

<a name="ref5"></a>[5] Finke MD. Energy requirements of adult female beagles. J. Nutr. 1994;124:2604S–2608S.

<a name="ref6"></a>[6] Kienzle E, Opitz B, Earle KE, et al. The development of an improved method of predicting the energy content in prepared dog and cat food. J Nutr 1998;128(suppl 12):2806S–2808S.

<a name="ref7"></a>[7] Michel KE Brown DC. Determination and application of cut points for accelerometer- based activity counts of activities with differing intensity in pet dogs. Am J Vet Res. 2011 Jul;72(7):866-70. doi: 10.2460/ajvr.72.7.866.

<a name="ref8"></a>[8] Emma N. Bermingham David G. Thomas Nicholas J. Cave Penelope J. Morris Richard F. Butterwick Alexander J. German. Energy Requirements of Adult Dogs: A Meta-Analysis PLoS ONE vol. 9 issue 10 (2014) pp: e109681

<a name="ref9"></a>[9] Ladha C, O’Sullivan J, Belshaw Z, Asher L. *A step in the right direction.* BMC Vet. Res. 2018;14:107.

<a name="ref10"></a>[10] Griffin TM, Main RP, Farley CT. *Biomechanics of quadrupedal walking: how do four-legged animals achieve inverted pendulum-like movements?* J. Exp. Biol. 2004;207:3545–58.

<a name="ref11"></a>[11] Hawthorne AJ, Booles D, Nugent PA, Gettinby G, Wilkinson J. *Body-Weight Changes during Growth in Puppies of Different Breeds.* Am. J. Nutr. Sci. 2004;4.

<a name="ref12"></a>[12] Neil Zhao. *Full Featured Pedometer Design Realized with 3-Axis Digital Accelerometer.* Analog Devices Application Note in Analoge-Dialoge June 2010. Available at http://www.analog.com/media/en/analog-dialogue/volume-44/number-2/articles/pedometer-design-3-axis-digital-acceler.pdf

<a name="ref13"></a>[13] VT van Hees, F Renström, A Wright, A Gradmark, M Catt, KY Chen, M Löf. *Estimation of daily energy expenditure in pregnant and non-pregnant women using a wrist-worn tri-axial accelerometer*. PloS one 6 (7), e22922

<a name="ref14"></a>[14] Morrison, R., Penpraze, V., Beber, A., Reilly, J. J. and Yam, P. S. *Associations between obesity and physical activity in dogs: a preliminary investigation.*  Journal of Small Animal Practice, 54: 570–574.

<a name="ref15"></a>[15] Yam P, Penpraze V. *Validity, practical utility and reliability of Actigraph accelerometry for the measurement of habitual physical activity in dogs.* J. Small Anim. Pract. 2011;52:86–91.

<a name="ref16"></a>[16] Morrison R, Reilly JJ, Penpraze V, Pendlebury E, Yam PS. *A 6-month observational study of changes in objectively measured physical activity during weight loss in dogs.* J. Small Anim. Pract. 2014;55:566–70.
